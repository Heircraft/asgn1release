/**
 * 
 */
package asgn1SoccerCompetition;

import java.util.ArrayList;
import java.util.Collections;

import asgn1Exceptions.CompetitionException;
import asgn1Exceptions.LeagueException;

/**
 * A class to model a soccer competition. The competition contains one or more number of leagues, 
 * each of which contain a number of teams. Over the course a season matches are played between
 * teams in each league. At the end of the season a premier (top ranked team) and wooden spooner 
 * (bottom ranked team) are declared for each league. If there are multiple leagues then relegation 
 * and promotions occur between the leagues.
 * 
 * @author Alan Woodley
 * @version 1.0
 *
 */
public class SoccerCompetition implements SportsCompetition{
	
	String name;
	int numLeagues; // make these two private 
	int numTeams;
	ArrayList<SoccerLeague> leagues = new ArrayList<SoccerLeague>();
	
	/**
	 * Creates the model for a new soccer competition with a specific name,
	 * number of leagues and number of teams in each league
	 * 
	 * @param name The name of the competition.
	 * @param numLeagues The number of leagues in the competition.
	 * @param numTeams The number of teams in each league.
	 */
	public SoccerCompetition(String name, int numLeagues, int numTeams){
	

		
		this.name = name;
		this.numLeagues = numLeagues;
		this.numTeams = numTeams;
		
		for (int i = 0; i < numLeagues; i++) {
			leagues.add(new SoccerLeague(numTeams));
		}
	}
	
	/**
	 * Retrieves a league with a specific number (indexed from 0). Returns an exception if the 
	 * league number is invalid.
	 * 
	 * @param leagueNum The number of the league to return.
	 * @return A league specified by leagueNum.
	 * @throws CompetitionException if the specified league number is less than 0.
	 *  or equal to or greater than the number of leagues in the competition.
	 */
	public SoccerLeague getLeague(int leagueNum) throws CompetitionException{
		if (leagueNum < 0 || leagueNum >= numLeagues) {
			throw new CompetitionException("Invalid league number");
		}
		return leagues.get(leagueNum);
		
	}
	

	/**
	 * Starts a new soccer season for each league in the competition.
	 */
	public void startSeason() {
		try {
			for (int i = 0; i < leagues.size(); i++) {
				leagues.get(i).startNewSeason();
			}
		} catch (LeagueException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	/** 
	 * Ends the season of each of the leagues in the competition. 
	 * If there is more than one league then it handles promotion
	 * and relegation between the leagues.  
	 * 
	 */
	public void endSeason()  {
		SoccerTeam sTTB; //temp for bot team
		SoccerTeam sTTT; // temp for top team
				
		for (int j = 0; j < leagues.size(); j++) {
			try {
				leagues.get(j).endSeason();		//ENDS THE SEASON FOR ALL TEAMS
			} catch (LeagueException e) {
				e.printStackTrace();
			}											
		}
		
		  for (int i = 0; i < leagues.size() - 1; i++) {
			SoccerTeam bTeam = leagues.get(i).teams.get(leagues.get(i).teams.size() - 1);
			SoccerTeam tTeam = leagues.get(i+1).teams.get(0);
			
			
			if (bTeam.compareTo(tTeam) >= 1) {  //if the lower team has a higher score than higher team
				
				sTTB = bTeam;
				sTTT = tTeam;
			
				leagues.get(i).teams.remove(leagues.get(i).teams.get(leagues.get(i).teams.size() - 1));
				leagues.get(i+1).teams.remove(leagues.get(i+1).teams.get(0));
				
				leagues.get(i+1).teams.add(leagues.get(i+1).teams.size()-1 , sTTB); 
				leagues.get(i).teams.add( 0, sTTT); 
				
			}	

		}
	}


	/** 
	 * For each league displays the competition standings.
	 */
	public void displayCompetitionStandings(){
		System.out.println("+++++" + this.name + "+++++");
		for (int i = 0; i < numLeagues; i++) { //THIS ITERATES HOWEVER MANY LEAGUES THERE ARE
		
			System.out.println("---- League" + (i +1) + " ----");
			System.out.println("Official Name" +  '\t' +  "Nick Name" + '\t' + "Form" + '\t' +  "Played" + '\t' + "Won" + '\t' + "Lost" + '\t' + "Drawn" + '\t' + "For" + '\t' + "Against" + '\t' + "GlDiff" + '\t' + "Points");
		//	for (int j = 0; j < numTeams; j++) {//THIS ITERATES HOWEVER MANY TEAMS THERE ARE
				leagues.get(i).displayLeagueTable();
			//}
		}
	
	}
	

}
