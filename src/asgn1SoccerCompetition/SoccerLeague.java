package asgn1SoccerCompetition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import asgn1Exceptions.LeagueException;
import asgn1Exceptions.TeamException;
import asgn1SportsUtils.WLD;

/**
 * A class to model a soccer league. Matches are played between teams and points awarded for a win,
 * loss or draw. After each match teams are ranked, first by points, then by goal difference and then
 * alphabetically. 
 * 
 * @author Alan Woodley
 * @version 1.0
 *
 */
public class SoccerLeague implements SportsLeague{
	// Specifies the number of team required/limit of teams for the league
	private int requiredTeams;
	// Specifies is the league is in the off season
	private boolean offSeason; //MIGHT BE WRONG
	public ArrayList<SoccerTeam> teams = new ArrayList<SoccerTeam>(requiredTeams);
	
	
	/**
	 * Generates a model of a soccer team with the specified number of teams. 
	 * A season can not start until that specific number of teams has been added. 
	 * Once that number of teams has been reached no more teams can be added unless
	 * a team is first removed. 
	 * 
	 * @param requiredTeams The number of teams required/limit for the league.
	 */
	public SoccerLeague (int requiredTeams){
		this.requiredTeams = requiredTeams;
		offSeason = true;
	}

	
	/**
	 * Registers a team to the league.
	 * 
	 * @param team Registers a team to play in the league.
	 * @throws LeagueException If the season has already started, if the maximum number of 
	 * teams allowed to register has already been reached or a team with the 
	 * same official name has already been registered.
	 */
	public void registerTeam(SoccerTeam team) throws LeagueException {
		// Just use contains team for this if you want 
		for (int i = 0; i < teams.size(); i++) {
			if (teams.get(i).getOfficialName() == team.getOfficialName()) {
				throw new LeagueException("there is already a team with that name");
			}
		}
		if (isOffSeason() == false) {
			throw new LeagueException("The season has already started");
		} else if (teams.size() >= requiredTeams) {
			throw new LeagueException("There are already enough teams in the league");
		} else if (teams.size() < requiredTeams) {
			teams.add(team);
		}
	}
	
	/**
	 * Removes a team from the league.
	 * 
	 * @param team The team to remove
	 * @throws LeagueException if the season has not ended or if the team is not registered into the league.
	 */
	public void removeTeam(SoccerTeam team) throws LeagueException{
		if (teams.contains(team) == false) {
			throw new LeagueException("That team has not been registered into the league");
		} else if (isOffSeason() == false) {
			throw new LeagueException("The season has not ended yet");
		} else {
			teams.remove(team);
		}
	}
	
	/** 
	 * Gets the number of teams currently registered to the league
	 * 
	 * @return the current number of teams registered
	 */
	public int getRegisteredNumTeams(){
		return teams.size();
	}
	
	/**
	 * Gets the number of teams required for the league to begin its 
	 * season which is also the maximum number of teams that can be registered
	 * to a league.

	 * @return The number of teams required by the league/maximum number of teams in the league
	 */
	public int getRequiredNumTeams(){
		return requiredTeams;
	}
	
	/** 
	 * Starts a new season by reverting all statistics for each team to initial values.
	 * 
	 * @throws LeagueException if the number of registered teams does not equal the required number of teams or if the season has already started
	 */
	public void startNewSeason() throws LeagueException{
		
		if (teams.size() < requiredTeams) {
			throw new LeagueException("There are not enough teams enrolled in the league yet");
		} else if (isOffSeason() == false) {
			throw new LeagueException("The season has already started");
		} else {
			for (int i = 0; i < teams.size(); i++)  {
				teams.get(i).resetStats();
			}
			offSeason = false;
		}
	}
	

	/**
	 * Ends the season.
	 * 
	 * @throws LeagueException if season has not started
	 */
	public void endSeason() throws LeagueException{
		if (isOffSeason() == true) {
			throw new LeagueException("The league is already in off season");
		} else {
			offSeason = true;
		}	
	}
	
	/**
	 * Specifies if the league is in the off season (i.e. when matches are not played).
	 * @return True If the league is in its off season, false otherwise.
	 */
	public boolean isOffSeason(){
		return this.offSeason;
	}
	
	
	
	/**
	 * Returns a team with a specific name.
	 * 
	 * @param name The official name of the team to search for.
	 * @return The team object with the specified official name.
	 * @throws LeagueException if no team has that official name.
	 */
	public SoccerTeam getTeamByOfficalName(String name) throws LeagueException{		
		int k = 0;
		for (int j = 0; j < teams.size(); j++) {
			if (teams.get(j).getOfficialName() == name) {
				break;
			} 
			k +=1;
		}
		if (k == teams.size()) {
			throw new LeagueException("There is no team with that official name");
		}
		
		for (int i = 0; i < teams.size(); i++) {
			if (teams.get(i).getOfficialName().equals(name)) {
				return teams.get(i);
			}
		}
		return null; 
	}
		
	/**
	 * Plays a match in a specified league between two teams with the respective goals. After each match the teams are
	 * re-sorted.
     *
	 * @param homeTeamName The name of the home team.
	 * @param homeTeamGoals The number of goals scored by the home team.
	 * @param awayTeamName The name of the away team.
	 * @param awayTeamGoals The number of goals scored by the away team.
	 * @throws LeagueException If the season has not started or if both teams have the same official name. 
	 */
	public void playMatch(String homeTeamName, int homeTeamGoals, String awayTeamName, int awayTeamGoals) throws LeagueException{
		if (isOffSeason() == true) {
			throw new LeagueException("The league is already in off season");
		} else if (homeTeamName == awayTeamName) {
			throw new LeagueException("Both teams have the same official name");
		}
		
		
		
		
		//CALLS UPON PLAYMATCH IN SOCCERTEAM AND USES IT FOR BOTH TEAMS
		for (int i = 0; i < teams.size(); i++) {
			
			try {
				if (teams.get(i).getOfficialName().equals(homeTeamName)) {
					teams.get(i).playMatch(homeTeamGoals,awayTeamGoals);
				}
			} catch (TeamException e) {
				e.printStackTrace();
			}
		}
		
		
		for (int i = 0; i < teams.size(); i++) {
			try {
				if (teams.get(i).getOfficialName().equals(awayTeamName)) {
					teams.get(i).playMatch(awayTeamGoals,homeTeamGoals);
				}
			} catch (TeamException e) {
				e.printStackTrace();
			}
		}
		
		
		
		
		sortTeams();
	}
	
	/**
	 * Displays a ranked list of the teams in the league  to the screen.
	 */
	public void displayLeagueTable(){
		sortTeams(); 
		
		
		for (int i = 0; i < teams.size(); i++) {
			teams.get(i).displayTeamDetails();
		}
		
	}	
	
	/**
	 * Returns the highest ranked team in the league.
     *
	 * @return The highest ranked team in the league. 
	 * @throws LeagueException if the number of teams is zero or less than the required number of teams.
	 */
	public SoccerTeam getTopTeam() throws LeagueException{
		if (teams.size() == 0) {
			throw new LeagueException("There are no teams in the league");
		} else if (teams.size() < requiredTeams) {
			throw new LeagueException("There are not yet enough teams in the league");
		}
		
		sortTeams();
		return teams.get(0);
	}

	/**
	 * Returns the lowest ranked team in the league.
     *
	 * @return The lowest ranked team in the league. 
	 * @throws LeagueException if the number of teams is zero or less than the required number of teams.
	 */
	public SoccerTeam getBottomTeam() throws LeagueException{
		if (teams.size() == 0) {
			throw new LeagueException("There are no teams in the league");
		} else if (teams.size() < requiredTeams) {
			throw new LeagueException("There are not yet enough teams in the league");
		}
		sortTeams();
		return teams.get(teams.size() - 1);
	}

	/** 
	 * Sorts the teams in the league.
	 */
    public void sortTeams(){	
    	Collections.sort(teams);
    }
    
    /**
     * Specifies if a team with the given official name is registered to the league.
     * 
     * @param name The name of a team.
     * @return True if the team is registered to the league, false otherwise. 
     */
    public boolean containsTeam(String name){
		if (teams.contains(name) == true) {
			return true;
		} else {
			return false;
		}
    }
    
}
