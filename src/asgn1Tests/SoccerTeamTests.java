package asgn1Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import asgn1Exceptions.TeamException;
import asgn1SoccerCompetition.SoccerTeam;
import asgn1SportsUtils.WLD;



/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerLeage class
 *
 * @author Alan Woodley
 *
 */
public class SoccerTeamTests {
	

	
	
	@Test (expected = TeamException.class)
	public void checkIfExceptionThrownWhenNull() throws TeamException {
		SoccerTeam newbies = new SoccerTeam(null, null);	
	}
	
	@Test
	public void checkIfGetOfficialNameReturns() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		assertEquals(test.getOfficialName(),"officename");
	}
	
	@Test
	public void checkIfGetNickNameReturns() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		assertEquals(test.getNickName(),"nicker");
	}
	
	@Test
	public void checkIfGetSGoalsScoredSeasonReturns() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		test.playMatch(5,7);
		assertEquals(test.getGoalsScoredSeason(), 5);
	}
	
	@Test
	public void checkIfGetSGoalsConcededSeasonReturns() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		test.playMatch(5,7);
		assertEquals(test.getGoalsConcededSeason(), 7);
	}
	
	@Test
	public void checkIfGetMatchesWonReturns() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		test.playMatch(10,7);
		test.playMatch(9,7);
		test.playMatch(8,7);
		test.playMatch(7,7);
		test.playMatch(7,7);
		test.playMatch(6,7);
		assertEquals(test.getMatchesWon(), 3);
	}
	
	@Test
	public void checkIfGetMatchesLostReturns() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		test.playMatch(10,7);
		test.playMatch(9,7);
		test.playMatch(8,7);
		test.playMatch(7,7);
		test.playMatch(7,7);
		test.playMatch(6,7);
		assertEquals(test.getMatchesLost(), 1);
	}
	
	@Test
	public void checkIfGettMatchesDrawnReturns() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		test.playMatch(10,7);
		test.playMatch(9,7);
		test.playMatch(8,7);
		test.playMatch(7,7);
		test.playMatch(7,7);
		test.playMatch(6,7);
		assertEquals(test.getMatchesDrawn(), 2);
	}
	
	@Test
	public void checkIfGetCompetitionPointsReturns() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		test.playMatch(10,7);
		test.playMatch(9,7);
		test.playMatch(8,7);
		test.playMatch(7,7);
		test.playMatch(7,7);
		test.playMatch(6,7);
		assertEquals(test.getCompetitionPoints(), 11);
	}
	
	@Test
	public void checkIfGetGoalDifferenceReturns() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		test.playMatch(10,7);
		test.playMatch(9,7);
		test.playMatch(8,7);
		test.playMatch(7,3);
		test.playMatch(7,2);
		test.playMatch(6,3);
		assertEquals(test.getGoalsScoredSeason() - test.getGoalsConcededSeason(), 18);
	}
	
	@Test
	public void checkIfGetFormStringReturns() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		test.playMatch(10,7);
		test.playMatch(7,3);
		test.playMatch(2,7);
		test.playMatch(6,6);
		test.playMatch(6,6);
		assertEquals(test.getFormString(), "DDLWW");
	}
	
	@Test (expected = TeamException.class)
	public void checkIfPlayMatchThrowsException() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		test.playMatch(-1, 3);
	}
	
	@Test (expected = TeamException.class)
	public void checkIfPlayMatchThrowsException2() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		test.playMatch(21, 3);
	}
	
	@Test (expected = TeamException.class)
	public void checkIfPlayMatchThrowsException3() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		test.playMatch(3, -1);
	}
	
	@Test (expected = TeamException.class)
	public void checkIfPlayMatchThrowsException4() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		test.playMatch(3, 21);
	}
	
	@Test
	public void checkIfCompareToReturnspos() throws TeamException {
		SoccerTeam test1 = new SoccerTeam("officename", "nicker");
		SoccerTeam test2 = new SoccerTeam("lukkerdogs", "doge");
		test1.playMatch(5,0);
		test2.playMatch(5, 0);
		assertTrue(test1.compareTo(test2) >= 1);
	}			
	
	@Test
	public void checkIfCompareToReturns1neg() throws TeamException {
		SoccerTeam test1 = new SoccerTeam("officename", "nicker");
		SoccerTeam test2 = new SoccerTeam("lukkerdogs", "doge");
		test1.playMatch(5,0);
		test2.playMatch(5, 0);
		assertTrue(test2.compareTo(test1) <= 1);
	}
	
	@Test
	public void checkIfCompareToReturns2pos() throws TeamException {
		SoccerTeam test1 = new SoccerTeam("officename", "nicker");
		SoccerTeam test2 = new SoccerTeam("lukkerdogs", "doge");
		test1.playMatch(5,0);
		test2.playMatch(5, 3);
		assertTrue(test2.compareTo(test1) >= 1);
	}	
	
	@Test
	public void checkIfCompareToReturn2neg() throws TeamException {
		SoccerTeam test1 = new SoccerTeam("officename", "nicker");
		SoccerTeam test2 = new SoccerTeam("lukkerdogs", "doge");
		test1.playMatch(5,3);
		test2.playMatch(5, 0);
		assertTrue(test2.compareTo(test1) <= 1);
	}	
	
	@Test
	public void checkIfCompareToReturns3pos() throws TeamException {
		SoccerTeam test1 = new SoccerTeam("officename", "nicker");
		SoccerTeam test2 = new SoccerTeam("lukkerdogs", "doge");
		test1.playMatch(5,3);
		test2.playMatch(3, 5);
		assertTrue(test2.compareTo(test1) >= 1);
	}	
	
	@Test
	public void checkIfCompareToReturns3neg() throws TeamException {
		SoccerTeam test1 = new SoccerTeam("officename", "nicker");
		SoccerTeam test2 = new SoccerTeam("lukkerdogs", "doge");
		test1.playMatch(3,5);
		test2.playMatch(5, 3);
		assertTrue(test2.compareTo(test1) <= 1);
	}	
	
	@Test
	public void checkIfRestStatsResets() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		assertEquals(test.getGoalsScoredSeason(), 0);
	}	
	
	@Test
	public void checkIfRestStatsResets1() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		assertEquals(test.getGoalsConcededSeason(), 0);
	}	
	
	@Test
	public void checkIfRestStatsResets3() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		assertEquals(test.getMatchesLost(), 0);
	}	
	
	@Test
	public void checkIfRestStatsResets4() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		assertEquals(test.getMatchesWon(), 0);
	}	
	
	@Test
	public void checkIfRestStatsResets5() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		assertEquals(test.getMatchesDrawn(), 0);
	}	
	
	@Test
	public void checkIfRestStatsResets6() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		assertEquals(test.getCompetitionPoints(), 0);
	}	
	
	@Test
	public void checkIfRestStatsResets7() throws TeamException {
		SoccerTeam test = new SoccerTeam("officename", "nicker");
		test.playMatch(10,7);
		test.playMatch(7,3);
		test.playMatch(2,7);
		test.playMatch(6,6);
		test.playMatch(6,6);
		test.resetStats();
		assertTrue(test.getFormString().equals("-----"));
	}	
	
	
	
	
	
	
	
	
	
	
	
	

}
