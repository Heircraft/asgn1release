package asgn1Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import asgn1Exceptions.LeagueException;
import asgn1Exceptions.TeamException;
import asgn1SoccerCompetition.SoccerLeague;
import asgn1SoccerCompetition.SoccerTeam;
import asgn1SportsUtils.WLD;


/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerLeage class
 *
 * @author Alan Woodley
 *
 */
public class SoccerLeagueTests {

	
	SoccerLeague test = new SoccerLeague(4);
	
	@Test (expected = LeagueException.class)
	public void checkIfRegisterTeamThrowsException() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("official","nick");	
		SoccerTeam newbies2 = new SoccerTeam("official","nick");	
		test.registerTeam(newbies);
		test.registerTeam(newbies2);
		
	}
	
	@Test (expected = LeagueException.class)
	public void checkIfRegisterTeamThrowsException2() throws LeagueException, TeamException {
		SoccerTeam newbies = new SoccerTeam("official","nick");	
		test.startNewSeason();
		test.registerTeam(newbies);
	
	}
	
	@Test (expected = LeagueException.class)
	public void checkIfRegisterTeamThrowsException3() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("1official","nick");	
		SoccerTeam newbies1 = new SoccerTeam("o2fficial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("of3ficial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("off4icial","nick");	
		SoccerTeam newbies4 = new SoccerTeam("offi5cial","nick");
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		test.registerTeam(newbies4);

	}
	
	@Test
	public void checkIfRegisterTeamAddsTeam() throws LeagueException, TeamException {
		SoccerTeam newbies = new SoccerTeam("official","nick");	
		test.registerTeam(newbies);
		assertTrue(test.teams.size() == 1);
	}
	
	@Test (expected = LeagueException.class)
	public void checkIfRemoveTeamThrowsException() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("1official","nick");	
		SoccerTeam newbies1 = new SoccerTeam("o2fficial","nick");
		test.registerTeam(newbies);
		test.removeTeam(newbies1);
	}
	
	@Test (expected = LeagueException.class)
	public void checkIfRemoveTeamThrowsException2() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("1official","nick");	
		SoccerTeam newbies1 = new SoccerTeam("o2fficial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("of3ficial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("off4icial","nick");	
		SoccerTeam newbies4 = new SoccerTeam("offi5cial","nick");
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		test.startNewSeason();
		test.removeTeam(newbies);

	}
	
	@Test 
	public void checkIfRegisterTeamReturns() throws LeagueException, TeamException {
		SoccerTeam newbies = new SoccerTeam("official","nick");	
		test.registerTeam(newbies);
		test.removeTeam(newbies);
		assertTrue(test.teams.size() == 0);
	}
	
	@Test 
	public void checkIfGetRegisteredNumTeamsReturns() throws LeagueException, TeamException {
		SoccerTeam newbies = new SoccerTeam("official","nick");	
		test.registerTeam(newbies);
		assertEquals(test.teams.size(), 1);
	}
	
	@Test 
	public void checkIfGetRequiredNumTeamsReturns() throws LeagueException, TeamException {
		assertEquals(test.getRequiredNumTeams(), 4);
	}
	
	@Test (expected = LeagueException.class)
	public void checkIfStartNewSeasonThrowsException() throws TeamException, LeagueException {
		SoccerTeam newbies1 = new SoccerTeam("o2fficial","nick");		
		SoccerTeam newbies4 = new SoccerTeam("offi5cial","nick");
		test.registerTeam(newbies4);
		test.registerTeam(newbies1);
		
		test.startNewSeason();

	}
	
	@Test (expected = LeagueException.class)
	public void checkIfStartNewSeasonThrowsException2() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("1official","nick");	
		SoccerTeam newbies1 = new SoccerTeam("o2fficial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("of3ficial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("off4icial","nick");	
		SoccerTeam newbies4 = new SoccerTeam("offi5cial","nick");
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		test.startNewSeason();
		test.startNewSeason();
	
	}
	
	
	
	@Test
	public void checkIfRestStatsResets() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("offi4cial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("offic5ial","nick");	
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		test.startNewSeason();
		test.teams.get(0).playMatch(5, 3);
		test.teams.get(0).playMatch(5, 5);
		test.teams.get(0).playMatch(5, 8);
		test.endSeason();
		test.startNewSeason();
		assertEquals(test.teams.get(0).getGoalsScoredSeason(), 0);
	}	
	
	@Test
	public void checkIfStartNewSeasonResets1() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("offi4cial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("offic5ial","nick");	
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		test.startNewSeason();
		test.teams.get(0).playMatch(5, 3);
		test.teams.get(0).playMatch(5, 5);
		test.teams.get(0).playMatch(5, 8);
		test.endSeason();
		test.startNewSeason();
		
		assertEquals(test.teams.get(0).getGoalsConcededSeason(), 0);
	}	
	
	@Test
	public void checkIfStartNewSeasonResets3() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("offi4cial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("offic5ial","nick");	
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		test.startNewSeason();
		test.teams.get(0).playMatch(5, 3);
		test.teams.get(0).playMatch(5, 5);
		test.teams.get(0).playMatch(5, 8);
		test.endSeason();
		test.startNewSeason();
		
		assertEquals(test.teams.get(0).getMatchesLost(), 0);
	}	
	
	@Test
	public void checkIfStartNewSeasonResets4() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("offi4cial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("offic5ial","nick");	
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		test.startNewSeason();
		test.teams.get(0).playMatch(5, 3);
		test.teams.get(0).playMatch(5, 5);
		test.teams.get(0).playMatch(5, 8);
		test.endSeason();
		test.startNewSeason();
		
		assertEquals(test.teams.get(0).getMatchesWon(), 0);
	}	
	
	@Test
	public void checkIfStartNewSeasonResets5() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("offi4cial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("offic5ial","nick");	
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		test.startNewSeason();
		test.teams.get(0).playMatch(5, 3);
		test.teams.get(0).playMatch(5, 5);
		test.teams.get(0).playMatch(5, 8);
		test.endSeason();
		test.startNewSeason();
		
		assertEquals(test.teams.get(0).getMatchesDrawn(), 0);
	}	
	
	@Test
	public void checkIfStartNewSeasonResets6() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("offi4cial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("offic5ial","nick");	
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		test.startNewSeason();
		test.teams.get(0).playMatch(5, 3);
		test.teams.get(0).playMatch(5, 5);
		test.teams.get(0).playMatch(5, 8);
		test.endSeason();
		test.startNewSeason();
		
		assertEquals(test.teams.get(0).getCompetitionPoints(), 0);
	}	
	
	@Test (expected = LeagueException.class)
	public void checkEndSeasonThrowsException() throws TeamException, LeagueException {
		test.endSeason();
		test.endSeason();
	}	

	@Test
	public void checkIfisOffSeasonReturns() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("offi4cial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("offic5ial","nick");	
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		
		test.startNewSeason();
		assertTrue(test.isOffSeason() == false);
	}	
	
	@Test (expected = LeagueException.class)
	public void checkIfGetTeamByOfficialNameThrowsException() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");		
		test.registerTeam(newbies);
		test.getTeamByOfficalName(newbies1.getOfficialName());
		
	}	
	
	@Test 
	public void checkIfGetTeamByOfficialNameReturns() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");		
		test.registerTeam(newbies);
		assertTrue(test.getTeamByOfficalName(newbies.getOfficialName()).equals(newbies));
	}	
	
	@Test (expected = LeagueException.class)
	public void checkIfPlayMatchThrowsException() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		
	test.playMatch(newbies.getOfficialName(),1,newbies1.getOfficialName(),1);
		
	}	
	
	@Test (expected = LeagueException.class)
	public void checkIfPlayMatchThrowsException2() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("offi4cial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("offic5ial","nick");	
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		
		test.startNewSeason();
	
		test.playMatch(newbies.getOfficialName(),1,newbies.getOfficialName(),1);
		
	}
	
	@Test 
	public void checkIfPlayMatchPlaysMatchCorrectly() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("offi4cial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("offic5ial","nick");	
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		
		test.startNewSeason();
	
		test.playMatch(newbies.getOfficialName(),1,newbies3.getOfficialName(),3);
		assertEquals(test.getTeamByOfficalName("off2icial").getGoalsScoredSeason(), 1);
	}
	
	@Test 
	public void checkIfPlayMatchPlaysMatchCorrectly2() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("offi4cial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("offic5ial","nick");	
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		
		test.startNewSeason();
	
		test.playMatch(newbies.getOfficialName(),1,newbies3.getOfficialName(),3);
		assertEquals(test.getTeamByOfficalName("off2icial").getGoalsConcededSeason(), 3);
	}
	
	@Test 
	public void checkIfPlayMatchPlaysMatchCorrectly3() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("offi4cial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("offic5ial","nick");	
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		
		test.startNewSeason();
	
		test.playMatch(newbies.getOfficialName(),1,newbies3.getOfficialName(),3);
		assertEquals(test.getTeamByOfficalName("offic5ial").getGoalsScoredSeason(), 3);
	}
	
	@Test 
	public void checkIfPlayMatchPlaysMatchCorrectly4() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("offi4cial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("offic5ial","nick");	
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		
		test.startNewSeason();
	
		test.playMatch(newbies.getOfficialName(),1,newbies3.getOfficialName(),3);
		assertEquals(test.getTeamByOfficalName("offic5ial").getGoalsConcededSeason(), 1);
	}
	
	@Test 
	public void checkIfPlayMatchPlaysMatchCorrectly5() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");	
		SoccerTeam newbies1 = new SoccerTeam("off3icial","nick");	
		SoccerTeam newbies2 = new SoccerTeam("offi4cial","nick");	
		SoccerTeam newbies3 = new SoccerTeam("offic5ial","nick");	
		test.registerTeam(newbies);
		test.registerTeam(newbies1);
		test.registerTeam(newbies2);
		test.registerTeam(newbies3);
		
		test.startNewSeason();
	
		test.playMatch(newbies.getOfficialName(),1,newbies3.getOfficialName(),3);
		assertEquals(test.getTeamByOfficalName("offic5ial").getGoalsConcededSeason(), 1);
	}
	
	@Test 
	public void checkIfDisplayLeagueTableSorts() throws TeamException, LeagueException {
		SoccerTeam newbies = new SoccerTeam("off2icial","nick");		
		test.registerTeam(newbies);
	
		//assertEquals(test. displayLeagueTable(), officialName  + '\t' + nickName + '\t' + form + '\t' + (+ matchesWon + matchesLost + matchesDrawn) + '\t' + matchesWon + '\t' + matchesLost + '\t' + matchesDrawn +'\t' + goalsScoredSeason + '\t' + goalsConcededSeason + '\t' + this.getGoalDifference() + '\t' + competitionPoints);
	}
}

