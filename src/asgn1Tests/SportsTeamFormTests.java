package asgn1Tests;

import static org.junit.Assert.*;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

import asgn1SoccerCompetition.SportsTeamForm;
import asgn1SportsUtils.WLD;

/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerTeamForm class
 *
 * @author Alan Woodley
 *
 */
public class SportsTeamFormTests {
	SportsTeamForm test = new SportsTeamForm();
	
	@Test
	public void checkIfResultIsAddedToFormWhenEmpty() {
		test.results.clear();
		test.addResultToForm(WLD.WIN);
		assertEquals(test.results.get(0).toString(), "WIN");
	}
	
	@Test
	public void checkIfResultIsRemovedWhenFull() {
		test.results.clear();
		test.addResultToForm(WLD.LOSS);
		test.addResultToForm(WLD.WIN);
		test.addResultToForm(WLD.DRAW);// This should be first result if working correctly
		test.addResultToForm(WLD.WIN);
		test.addResultToForm(WLD.LOSS); 
		test.addResultToForm(WLD.WIN);
		test.addResultToForm(WLD.DRAW);
		assertEquals(test.results.get(0).toString(), "DRAW");
	}
	
	@Test
	public void checkIftoStringReturnsCorrect() {
		test.results.clear();
		test.addResultToForm(WLD.WIN);
		test.addResultToForm(WLD.LOSS);
		test.addResultToForm(WLD.DRAW);
		test.addResultToForm(WLD.WIN);
		test.addResultToForm(WLD.LOSS);
		
		assertTrue(test.toString().equals("LWDLW"));
	}
	
	@Test
	public void checkIfFormSizeMaxesOutAt5() {
		test.results.clear();
		test.addResultToForm(WLD.WIN);
		test.addResultToForm(WLD.LOSS);
		test.addResultToForm(WLD.DRAW);
		test.addResultToForm(WLD.WIN);
		test.addResultToForm(WLD.LOSS);
		test.addResultToForm(WLD.LOSS);
		test.addResultToForm(WLD.WIN);
		
		assertEquals(test.toString().length(),5);
	}

	
	@Test
	public void checkIfArrayHasDashesWhenNull() {
		test.results.clear();
	
		assertTrue(test.toString().equals("-----"));
	}
	
	@Test
	public void checkIfGetNumGamesReturnsArraySize() {
	
		assertEquals(test.getNumGames(),test.results.size());
	}
	
	@Test
	public void checkIfResetFormClearsResults() {
		test.addResultToForm(WLD.DRAW);
		test.addResultToForm(WLD.WIN);
		test.resetForm();
		assertTrue(test.results.isEmpty());
	}
}